import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import 'normalize.css'
import firebase from "firebase/app"

Vue.config.productionTip = false

const firebaseConfig = {
    apiKey: "AIzaSyDHUEQ1Zz3gODde3jzrYdMTZFV-NQV8rH0",
    authDomain: "habit-tracker-272711.firebaseapp.com",
    databaseURL: "https://habit-tracker-272711.firebaseio.com",
    projectId: "habit-tracker-272711",
    storageBucket: "habit-tracker-272711.appspot.com",
    messagingSenderId: "170441077662",
    appId: "1:170441077662:web:be868be4f5d9a11dff9c8f",
    measurementId: "G-KCL9Z34EGC"
};

firebase.initializeApp(firebaseConfig)

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
