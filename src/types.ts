import {Vue} from "vue-property-decorator"

export enum PageMode {
    singlePage,
    dualPage
}

export class TypesGetters extends Vue{
    get PageMode() {
        return PageMode
    }
}